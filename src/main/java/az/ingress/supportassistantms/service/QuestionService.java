package az.ingress.supportassistantms.service;

import az.ingress.supportassistantms.dto.QuestionRequest;
import az.ingress.supportassistantms.dto.QuestionResponse;
import az.ingress.supportassistantms.entity.Question;
import az.ingress.supportassistantms.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class QuestionService {

    public final QuestionRepository questionRepository;
    private final ModelMapper modelMapper;

    public QuestionResponse addQuestion(QuestionRequest request) {
        Question question = modelMapper.map(request, Question.class);
        if (request.getParentId() != null) {
            Question parentQuestion = questionRepository.findById(request.getParentId())
                    .orElseThrow(() -> new RuntimeException("Parent question is not found"));
            question.setParent(parentQuestion);
        }
        Question savedQuestion = questionRepository.save(question);
        return modelMapper.map(savedQuestion, QuestionResponse.class);
    }

    public List<QuestionResponse> getAllRootQuestions() {
        List<Question> rootQuestions = questionRepository.findByParentIsNull();
        return rootQuestions.stream()
                .map(question -> modelMapper.map(question, QuestionResponse.class))
                .collect(Collectors.toList());
    }

    public List<QuestionResponse> getSubQuestions(Long parentId) {
        List<Question> subQuestions = questionRepository.findByParentId(parentId);
        return subQuestions.stream()
                .map(question -> modelMapper.map(question, QuestionResponse.class))
                .collect(Collectors.toList());
    }

    public List<QuestionResponse> getAllQuestions() {
        List<Question> questions = questionRepository.findAll();
        return questions.stream()
                .map(question -> modelMapper.map(question, QuestionResponse.class))
                .collect(Collectors.toList());
    }

    public QuestionResponse getQuestionById(Long id) {
        Question question = fetchById(id);
        return modelMapper.map(question, QuestionResponse.class);
    }


    public QuestionResponse updateQuestion(Long id, QuestionRequest request) {
        Question existingQuestion = fetchById(id);
        modelMapper.map(request, existingQuestion);
        if (request.getText() != null) existingQuestion.setText(request.getText());
        if (request.isMainQuestion() != existingQuestion.isMainQuestion()){
            existingQuestion.setMainQuestion(request.isMainQuestion());
        }

        Question updatedQuestion = questionRepository.save(existingQuestion);

        return modelMapper.map(updatedQuestion, QuestionResponse.class);
    }

    public void deleteQuestion(Long id) {
        questionRepository.deleteById(id);
    }

    private Question fetchById(Long id){
        return questionRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Question not found"));
    }
}
