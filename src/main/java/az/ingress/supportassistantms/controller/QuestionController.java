package az.ingress.supportassistantms.controller;

import az.ingress.supportassistantms.dto.QuestionRequest;
import az.ingress.supportassistantms.dto.QuestionResponse;
import az.ingress.supportassistantms.service.QuestionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/assistant/v1")
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionService questionService;

    @PostMapping
    public QuestionResponse addQuestion(@RequestBody QuestionRequest request){
        return questionService.addQuestion(request);
    }
    @GetMapping("/root")
    public ResponseEntity<List<QuestionResponse>> getAllRootQuestions() {
        List<QuestionResponse> rootQuestions = questionService.getAllRootQuestions();
        return new ResponseEntity<>(rootQuestions, HttpStatus.OK);
    }

    @GetMapping("/{parentId}/subquestions")
    public ResponseEntity<List<QuestionResponse>> getSubQuestions(@PathVariable Long parentId) {
        List<QuestionResponse> subQuestions = questionService.getSubQuestions(parentId);
        return new ResponseEntity<>(subQuestions, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<QuestionResponse>> getAllQuestions() {
        List<QuestionResponse> questions = questionService.getAllQuestions();
        return new ResponseEntity<>(questions, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionResponse> getQuestionById(@PathVariable Long id) {
        QuestionResponse question = questionService.getQuestionById(id);
        return new ResponseEntity<>(question, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<QuestionResponse> updateQuestion(@PathVariable Long id, @RequestBody QuestionRequest request) {
        QuestionResponse updatedQuestion = questionService.updateQuestion(id, request);
        return new ResponseEntity<>(updatedQuestion, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteQuestion(@PathVariable Long id) {
        questionService.deleteQuestion(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}

