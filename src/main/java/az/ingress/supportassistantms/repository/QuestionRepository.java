package az.ingress.supportassistantms.repository;

import az.ingress.supportassistantms.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
    List<Question> findByParentIsNull();
    List<Question> findByParentId(Long parentId);
}
