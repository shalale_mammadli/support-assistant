package az.ingress.supportassistantms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupportAssistantMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupportAssistantMsApplication.class, args);
    }

}
