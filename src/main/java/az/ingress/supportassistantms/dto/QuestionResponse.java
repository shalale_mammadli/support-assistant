package az.ingress.supportassistantms.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponse {
    private Long id;
    private String text;
    private Long parentId;
    private boolean isMainQuestion;
}
